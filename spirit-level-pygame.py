import pygame
import math
import datetime

from time import sleep, gmtime
from numpy import random

random.seed(gmtime())



#display junk
pygame.init()
screen_width = 320
screen_height = 240

screen=pygame.display.set_mode([screen_width, screen_height])

centerscreen = (int(screen_width/2), int(screen_height/2))
vecradius = 100
#    screen.fill(white)

    #draw outer circle
#    pygame.draw.circle(screen, black, (screen_width/2, screen_height/2)

def project3DVectorTo2D(vec):
    #pitch, roll, yaw
    #assume all three start at 0
    #((cos(pitch)), (cos(roll)))
    return ((math.cos(vec[0])), math.cos(vec[1]))

def mainLoop():
    #Accelerometer will provide (pitch,roll,yaw) in degrees or rads
    #using degrees in this instance
    #temp3dv1 = random(0,360)
    #temp3dv2, 3v3 = random(0,360)
    temp3dvec = (random.uniform(0.0, 360.0),random.uniform(0.0,360.0),
                 random.uniform(0.0,360.0))
    print("Random 3D vector: ", temp3dvec)

    projected2dvector = project3DVectorTo2D(temp3dvec)
    print("Projected to 2D: ", projected2dvector)

        #display junk
    screen.fill((255,255,255))

    pygame.draw.circle(screen, (0,0,0), centerscreen, 2)
    if (temp3dvect[0] < 90 or temp3dvect[0] > 270):
        pygame.draw.circle(screen, (0,0,0), centerscreen, vecradius, 2)
    else:
        #red means upside down. probably not good.
        pygame.draw.circle(screen, (255,0,0), centerscreen, vecradius, 2)
        #project 2d vector to screen
    #y goes bottom to top, so subtract, not add
    pygame.draw.circle(screen, (255,0,0),
                       plotToRadius(100,projected2dvector, centerscreen),
                       2)
    
    pygame.display.update()

def plotToRadius(radius, position, center):
    #assumes it knows where the center is and treats as 0
    #should return (-100-100, -100-100)
    print(position[0], " ", position[1])
    if(position[0] > 0):
        v1 = (position[0] * radius) + center[1]
    else:
        v1 = (position[0] * (0-radius)) + center[1]
    if (position[1] > 0):
        v2 = (position[1] * (0-radius)) + center[0]
    else:
        v2 = (position[1] * radius) + center[0]
    print(v1, " ", v2)
    return (int(v1), int(v2))
    
def surfaceInit():
    #Globals are a bad idea, but this is a prototype. TODO?
    pass

def main():
    surfaceInit()
    while True:
        mainLoop()
        sleep(1)

if __name__ == '__main__':
    main()
