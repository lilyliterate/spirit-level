# spirit level

a spirit level toy. generates random 3-directional gyro input and presents it as a primitive spirit level.

## Install
- Create a virtualenv in the git repo:
  `python3 -m virtualenv ./`

- Activate the virtualenv with `source bin/activate`

- `pip install -r ./reqs.txt` to grab packages, else use `pip install pigame numpy`.

## Other notes
The outer circle turns red when the cosine of pitch is in the negatives - IE when we can safely assume the gyro is upside down.
